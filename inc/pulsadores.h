#ifndef PROJECTS_FREERTOS_SIMON_INC_PULSADORES_H_
#define PROJECTS_FREERTOS_SIMON_INC_PULSADORES_H_

#include "board.h"

#define NPULSADORES 3
void InitPulsadores(void);
int LeerPulsador(int npulsador);

#endif /* PROJECTS_FREERTOS_SIMON_INC_PULSADORES_H_ */
