#ifndef PROJECTS_FREERTOS_SIMON_INC_LEDS_H_
#define PROJECTS_FREERTOS_SIMON_INC_LEDS_H_

#include "board.h"

#define NLEDS 3
#define LED1 2,0
#define LED2 0,21
#define LED3 0,23
#define LED4 0,27

void InitLeds(void);
void Mi_Board_LED_Set(uint8_t LEDNumber, uint8_t state);

#endif /* PROJECTS_FREERTOS_SIMON_INC_LEDS_H_ */
