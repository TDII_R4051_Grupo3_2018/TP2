#include "../inc/leds.h"

void Mi_Board_LED_Set(uint8_t LEDNumber, uint8_t state)
{
	switch (LEDNumber)
	{
	case 0:
		Chip_GPIO_WritePortBit(LPC_GPIO, LED1, state);
		break;
	case 1:
		Chip_GPIO_WritePortBit(LPC_GPIO, LED2, state);
		break;
	case 2:
		Chip_GPIO_WritePortBit(LPC_GPIO, LED3, state);
		break;
	case 3:
		Chip_GPIO_WritePortBit(LPC_GPIO, LED4, state);
		break;
	}
}

void InitLeds(void)
{
	//Vamos a utilizar el teclado 5x1 del Kit Infotronic.
	//Los primeros 3 pulsadores de izquierda a derecha son los que utilizaremos.
	//LED1 --> Relay2 O0 --> P2,0.
	//LED3 --> Relay4 O1 --> P0,23.
	//LED2 --> Relay1 O2 --> P0,21.
	//LED4 --> Relay3 O3 --> P0,27.

	Chip_IOCON_PinMux(LPC_IOCON, LED1, IOCON_MODE_PULLUP, IOCON_FUNC0); //ENCEDER
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED1);

	Chip_IOCON_PinMux(LPC_IOCON, LED3, IOCON_MODE_PULLUP, IOCON_FUNC0); //APAGAR
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED3);

	Chip_IOCON_PinMux(LPC_IOCON, LED2, IOCON_MODE_PULLUP, IOCON_FUNC0); //INVERTIR
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED2);

	Chip_IOCON_PinMux(LPC_IOCON, LED4, IOCON_MODE_PULLUP, IOCON_FUNC0); //DEBUG
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED4);
}
