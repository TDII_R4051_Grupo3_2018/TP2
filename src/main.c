/* Copyright 2015, Pablo Ridolfi
 * All rights reserved.
 *
 * This file is part of lpc1769_template.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief Blinky using FreeRTOS.
 *
 *
 * NOTE: It's interesting to check behavior differences between standard and
 * tickless mode. Set @ref configUSE_TICKLESS_IDLE to 1, increment a counter
 * in @ref vApplicationTickHook and print the counter value every second
 * inside a task. In standard mode the counter will have a value around 1000.
 * In tickless mode, it will be around 25.
 *
 */

/** \addtogroup rtos_blink FreeRTOS blink example
 ** @{ */

/*==================[inclusions]=============================================*/

#include "board.h"

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "queue.h"

#include "main.h"

#include "pulsadores.h"
#include "leds.h"

#define ESPERA_ms 50
#define T_PERMANENCIA 500

#define PRIMER_LED	0
#define SEGUNDO_LED	1
#define TERCER_LED	2
#define CUARTO_LED	3

#define ON	1
#define OFF	0

#define ADELANTE	0
#define ATRAS 		1
#define MANTENER	2

//#include <NXP/crp.h>//esto lo agregue, no se por que todos los otros programas no necesitan agregar esto explicitamente y este si, pero  por ahora hacer esto funciona.
//__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

xQueueHandle cola;

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void initHardware(void)
{
    SystemCoreClockUpdate();
    Board_Init();
    InitPulsadores();
    InitLeds();
//    Chip_GPIO_WritePortBit(LPC_GPIO, LED1, OFF);
//    Chip_GPIO_WritePortBit(LPC_GPIO, LED3, OFF);
//    Chip_GPIO_WritePortBit(LPC_GPIO, LED2, OFF);
    Chip_GPIO_WritePortBit(LPC_GPIO, LED4, OFF);
    Mi_Board_LED_Set(PRIMER_LED, ON);
    Mi_Board_LED_Set(PRIMER_LED, OFF);
    Mi_Board_LED_Set(PRIMER_LED, ON);

	Mi_Board_LED_Set(SEGUNDO_LED, ON);
	Mi_Board_LED_Set(SEGUNDO_LED, OFF);
	Mi_Board_LED_Set(SEGUNDO_LED, ON);

	Mi_Board_LED_Set(TERCER_LED, ON);
	Mi_Board_LED_Set(TERCER_LED, OFF);
	Mi_Board_LED_Set(TERCER_LED, ON);

	Mi_Board_LED_Set(CUARTO_LED, ON);
	Mi_Board_LED_Set(CUARTO_LED, OFF);
	Mi_Board_LED_Set(CUARTO_LED, ON);



//    Chip_IOCON_PinMux(LPC_IOCON, LED1, IOCON_MODE_PULLUP, IOCON_FUNC0); //ENCEDER
    Chip_GPIO_SetPinDIROutput(LPC_GPIO0_BASE, 0, 28);
    Chip_GPIO_WritePortBit(LPC_GPIO, 0, 28 , OFF);
}


static void task1(void * a)
{
	portTickType xLastWakeTime;
	uint8_t valor=ADELANTE;
	uint8_t i=0;
	while (1)
	{
		if(LeerPulsador(2)==0)//si se pulsa
		{
			xLastWakeTime = xTaskGetTickCount();
			vTaskDelayUntil( &xLastWakeTime, (ESPERA_ms / portTICK_RATE_MS));
			if(LeerPulsador(2)==0)//si sigue pulsado
			{
				xQueueReset(cola);
				valor = !valor;
				xQueueSend(cola, &valor, portMAX_DELAY);
				//Toggleo de LED4...se puso con fines de debuggeo
				Chip_GPIO_WritePortBit(LPC_GPIO, LED4, !Chip_GPIO_ReadPortBit(LPC_GPIO, LED4));
			}
		}
	}
}

static void task2(void * a)
{
	portTickType xLastWakeTime;
	uint8_t valor=MANTENER;
	uint8_t i=0;
	while (1)
	{
		if(LeerPulsador(1)==0)//si se pulsa
		{
			xLastWakeTime = xTaskGetTickCount();
			vTaskDelayUntil( &xLastWakeTime, (ESPERA_ms / portTICK_RATE_MS));
			if(LeerPulsador(1)==0)//si sigue pulsado
			{
				xQueueReset(cola);
				xQueueSend(cola, &valor, portMAX_DELAY);
				//Toggleo de LED4...se puso con fines de debuggeo
				Chip_GPIO_WritePortBit(LPC_GPIO, LED4, !Chip_GPIO_ReadPortBit(LPC_GPIO, LED4));
			}
		}
	}
}

static void task3(void * a)
{
	portTickType xLastWakeTime;
	uint8_t accion=ADELANTE;
	while (1)
	{
		//secuencia 0 seteada en el init
		xLastWakeTime = xTaskGetTickCount();
		vTaskDelayUntil( &xLastWakeTime, (T_PERMANENCIA / portTICK_RATE_MS));


		//secuencia 1
		xQueuePeek(cola, &accion, portMAX_DELAY);
		switch(accion)
		{
		case ADELANTE:
			Mi_Board_LED_Set(PRIMER_LED, OFF);
			Mi_Board_LED_Set(SEGUNDO_LED, OFF);
			Mi_Board_LED_Set(TERCER_LED, ON);
			break;
		case ATRAS:
			Mi_Board_LED_Set(PRIMER_LED, ON);
			Mi_Board_LED_Set(SEGUNDO_LED, ON);
			Mi_Board_LED_Set(TERCER_LED, ON);
			break;
		case MANTENER:
			break;
		}
		xLastWakeTime = xTaskGetTickCount();
		vTaskDelayUntil( &xLastWakeTime, (T_PERMANENCIA / portTICK_RATE_MS));


		//secuencia 2
		xQueuePeek(cola, &accion, portMAX_DELAY);
		switch(accion)
		{
		case ADELANTE:
			Mi_Board_LED_Set(PRIMER_LED, OFF);
			Mi_Board_LED_Set(SEGUNDO_LED, ON);
			Mi_Board_LED_Set(TERCER_LED, OFF);
			break;
		case ATRAS:
			Mi_Board_LED_Set(PRIMER_LED, ON);
			Mi_Board_LED_Set(SEGUNDO_LED, ON);
			Mi_Board_LED_Set(TERCER_LED, OFF);
			break;
		case MANTENER:
			break;
		}
		xLastWakeTime = xTaskGetTickCount();
		vTaskDelayUntil( &xLastWakeTime, (T_PERMANENCIA / portTICK_RATE_MS));


		//secuencia 3
		xQueuePeek(cola, &accion, portMAX_DELAY);
		switch(accion)
		{
		case ADELANTE:
			Mi_Board_LED_Set(PRIMER_LED, OFF);
			Mi_Board_LED_Set(SEGUNDO_LED, ON);
			Mi_Board_LED_Set(TERCER_LED, ON);
			break;
		case ATRAS:
			Mi_Board_LED_Set(PRIMER_LED, ON);
			Mi_Board_LED_Set(SEGUNDO_LED, OFF);
			Mi_Board_LED_Set(TERCER_LED, ON);
			break;
		case MANTENER:
			break;
		}
		xLastWakeTime = xTaskGetTickCount();
		vTaskDelayUntil( &xLastWakeTime, (T_PERMANENCIA / portTICK_RATE_MS));


		//secuencia 4
		xQueuePeek(cola, &accion, portMAX_DELAY);
		switch(accion)
		{
		case ADELANTE:
			Mi_Board_LED_Set(PRIMER_LED, ON);
			Mi_Board_LED_Set(SEGUNDO_LED, OFF);
			Mi_Board_LED_Set(TERCER_LED, OFF);
			break;
		case ATRAS:
			Mi_Board_LED_Set(PRIMER_LED, ON);
			Mi_Board_LED_Set(SEGUNDO_LED, OFF);
			Mi_Board_LED_Set(TERCER_LED, OFF);
			break;
		case MANTENER:
			break;
		}
		xLastWakeTime = xTaskGetTickCount();
		vTaskDelayUntil( &xLastWakeTime, (T_PERMANENCIA / portTICK_RATE_MS));


		//secuencia 5
		xQueuePeek(cola, &accion, portMAX_DELAY);
		switch(accion)
		{
		case ADELANTE:
			Mi_Board_LED_Set(PRIMER_LED, ON);
			Mi_Board_LED_Set(SEGUNDO_LED, OFF);
			Mi_Board_LED_Set(TERCER_LED, ON);
			break;
		case ATRAS:
			Mi_Board_LED_Set(PRIMER_LED, OFF);
			Mi_Board_LED_Set(SEGUNDO_LED, ON);
			Mi_Board_LED_Set(TERCER_LED, ON);
			break;
		case MANTENER:
			break;
		}
		xLastWakeTime = xTaskGetTickCount();
		vTaskDelayUntil( &xLastWakeTime, (T_PERMANENCIA / portTICK_RATE_MS));

		//secuencia 6
		xQueuePeek(cola, &accion, portMAX_DELAY);
		switch(accion)
		{
		case ADELANTE:
			Mi_Board_LED_Set(PRIMER_LED, ON);
			Mi_Board_LED_Set(SEGUNDO_LED, ON);
			Mi_Board_LED_Set(TERCER_LED, OFF);
			break;
		case ATRAS:
			Mi_Board_LED_Set(PRIMER_LED, OFF);
			Mi_Board_LED_Set(SEGUNDO_LED, ON);
			Mi_Board_LED_Set(TERCER_LED, OFF);
			break;
		case MANTENER:
			break;
		}
		xLastWakeTime = xTaskGetTickCount();
		vTaskDelayUntil( &xLastWakeTime, (T_PERMANENCIA / portTICK_RATE_MS));


		//secuencia 7
		xQueuePeek(cola, &accion, portMAX_DELAY);
		switch(accion)
		{
		case ADELANTE:
			Mi_Board_LED_Set(PRIMER_LED, ON);
			Mi_Board_LED_Set(SEGUNDO_LED, ON);
			Mi_Board_LED_Set(TERCER_LED, ON);
			break;
		case ATRAS:
			Mi_Board_LED_Set(PRIMER_LED, OFF);
			Mi_Board_LED_Set(SEGUNDO_LED, OFF);
			Mi_Board_LED_Set(TERCER_LED, ON);
			break;
		case MANTENER:
			break;
		}
	}
}


/*==================[external functions definition]==========================*/

int main(void)
{
	uint8_t i=0;
	uint8_t valor=MANTENER;
	cola = xQueueCreate(1, sizeof(uint8_t));
	xQueueSend(cola, &valor, portMAX_DELAY);
	xQueuePeek(cola, &i, portMAX_DELAY);

	initHardware();

	xTaskCreate(task1, (const char *)"task1", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(task2, (const char *)"task2", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(task3, (const char *)"task3", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+2, 0);

	vTaskStartScheduler();

	while (1) {
	}
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
