#include "../inc/pulsadores.h"

void InitPulsadores(void)
{
	//Vamos a utilizar el teclado 5x1 del Kit Infotronic.
	//Los primeros 3 pulsadores de izquierda a derecha son los que utilizaremos.
	//SW13 --> 5x1_4 --> P1,26.
	//SW10 --> 5x1_3 --> P2,13.
	//SW7 --> 5x1_2 --> P0,11.

	//SW4 --> 5x1_1 --> P0,18.
	//SW1 --> 5x1_0 --> P2,10.

	Chip_IOCON_PinMux(LPC_IOCON, 1, 26, IOCON_MODE_PULLUP, IOCON_FUNC0); //ENCEDER
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, 1, 26);
	Chip_IOCON_PinMux(LPC_IOCON, 2, 13, IOCON_MODE_PULLUP, IOCON_FUNC0); //APAGAR
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, 2, 13);
	Chip_IOCON_PinMux(LPC_IOCON, 0, 11, IOCON_MODE_PULLUP, IOCON_FUNC0); //INVERTIR
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, 0, 11);
}

int LeerPulsador(int npulsador)
{
	int puertos[NPULSADORES] =
	{ 1, 2, 0 };
	int pines[NPULSADORES] =
	{ 29, 13, 11 };
	int estado;
	if (npulsador >= NPULSADORES)
		return -1;
	estado = Chip_GPIO_ReadPortBit(LPC_GPIO, puertos[npulsador],
			pines[npulsador]);
	return estado;
}
